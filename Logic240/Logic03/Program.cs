﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukkan deret angka dgn pembatas tanda koma (,):");
            string deret = Console.ReadLine();
            string[] arrDeret = deret.Split(',');
            for (int i = 0; i < arrDeret.Length; i++)
            {
                Console.WriteLine(i + " : " + arrDeret[i]);
            }
            Console.Write("Press any key!");
            Console.ReadKey();
        }
    }
}
