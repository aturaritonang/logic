﻿using Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic01
{
    class Soal04: LogicProps
    {
        public Soal04(int n)
        {
            Array2D = new string[1, n];
            FillArray();
            LogicFunction.PrintArray(Array2D);
            Console.Write("Press any key to continue!!!");
            Console.ReadKey();
        }

        private void FillArray()
        {
            int val = 1;
            for (int col = 0; col < Array2D.GetLength(1); col++)
            {
                Array2D[0, col] = val.ToString();
                val = val + 3;
            }
        }
    }
}
